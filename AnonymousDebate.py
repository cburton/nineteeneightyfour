#!/usr/bin/env python
from configparser import ConfigParser
from os.path import isfile
from random import shuffle
import re

# global variables
_combinations = [(' ',' '),('\n',':')]
_punctuation = [str(chr(i)) for i in [33,34,39,40,41,44,45,46,58,59,63]]
_extra = ['...']
for char in _punctuation+_extra:
    _combinations.extend([(' ',char),(char,' ')])
_titles = ['Mr. ','Ms. ','Mrs. ', 'Dr. ','']

def readCfgFile(fileName):
    '''Read with configparser module and handle exceptions.'''
    config = ConfigParser() # open parser instance
    config.optionxform=str # parse as strings (case sensitivity)
    if not isfile(fileName): raise Exception('Configuration file "'+fileName+'" not found.')
    config.read(fileName)
    if not config.sections(): raise Exception('No sections in config file.')
    return config

def buildGroup(config, sectionName):
    '''Configure ReplacementGroup objects.'''
    if not config.has_section(sectionName): raise Exception('No section '+sectionName+' found.')
    configItems = dict(config.items(sectionName)) # build dict from list of tuples
    isHuman = '_NAME' in configItems
    isRand = '_RANDOM' in configItems
    isAscii = '_ID' in configItems and configItems['_ID']=='ASCII'
    baseName = configItems['_RANDOM'] if '_RANDOM' in configItems else ''
    return ReplacementGroup(configItems,baseName,isHuman,isRand,isAscii)

class Replacement:
    def __init__(self, specific, generic):
        self.specific = specific
        self.generic = generic
        self.specificList = self.specific.split('/')        
        self.buildReplacements()

    def buildReplacements(self):
        self.replaceDict = {}
        for specific in self.specificList:
            for pre,post in _combinations:
                self.replaceDict[pre+specific+post] = pre+self.generic+post

    def getReplacements(self):
        return self.replaceDict

class ReplacementHuman(Replacement):
    def __init__(self, specific, generic):
        super(ReplacementHuman,self).__init__(specific,generic)

    def buildReplacements(self):
        splitName = self.specific.split(',')
        firsts = splitName[0].split('/')
        lasts = splitName[1].split('/')
        
        allNames = [title+first+' '+last for first in firsts for last in lasts for title in _titles]
        allNames += [title+last for last in lasts for title in _titles]
        allNames += [first for first in firsts]
        allNames += [name.upper() for name in allNames]
        self.specificList = allNames
        super(ReplacementHuman,self).buildReplacements()

class ReplacementGroup:
    def __init__(self, specifics, genericBase='', isHuman=False, isRandom=False, isAscii=False):
        self.specifics = specifics # but, now exclude keywords
        keysToDrop = [specific for specific in specifics if specific[0] is '_']
        [self.specifics.pop(keyToDrop) for keyToDrop in keysToDrop]
        self.nKeys = len(self.specifics)

        self.genericBase = genericBase
        self.isHuman = isHuman
        self.isRandom = isRandom
        self.isAscii = isAscii

        self.buildPostfixes()
        self.buildGenerics()

        self.replacements = []
        for specific,generic in zip(self.specifics,self.generics):
            if isHuman: self.replacements.append(ReplacementHuman(specific,generic))
            else: self.replacements.append(Replacement(specific,generic))

    def buildPostfixes(self):
        if self.isAscii: self.postfixes = [chr(65+i) for i in range(self.nKeys)]
        else: self.postfixes = list(range(self.nKeys))

    def buildGenerics(self):
        self.generics = [self.genericBase+' '+str(postfix) for postfix in self.postfixes]
        if self.isRandom: shuffle(self.generics)

    def getReplacements(self):
        replaceDict = {}
        for replacement in self.replacements:
            replaceDict.update(replacement.getReplacements())
        return replaceDict

if __name__=='__main__':
    cfg = readCfgFile('Replacements.cfg')

    complete = {}
    for groupName in ['Candidates','States','Territories','Cities']:
         complete.update(buildGroup(cfg,groupName).getReplacements())
    for individualName in ['Other','Offices']:
        for item in cfg[individualName]:
            complete.update(Replacement(item,cfg[individualName][item]).getReplacements())

    with open('OriginalTranscript.txt','r') as oldFile: transcript = oldFile.read()
    for specific,generic in complete.items(): transcript = transcript.replace(specific,generic)
    with open('GenericTranscript.txt','w') as newFile: newFile.write(transcript)